use std::{
    cmp::Reverse,
    collections::BinaryHeap,
    env,
    fs::File,
    io::{self, BufRead, BufReader, BufWriter, Read, Seek, SeekFrom::Start, Write},
    mem,
    path::Path,
};

const RUN_SIZE: usize = 1024 * 1024 * 512; // 500 MB
const RUN_LEN: usize = RUN_SIZE / mem::size_of::<u32>();
const K_MERGE: usize = 3;

fn main() -> io::Result<()> {
    let fname = env::args().nth(1).unwrap();
    let source = BufReader::new(File::open(fname).unwrap());
    let mut lines = source
        .lines()
        .map(|r| r.unwrap())
        .map(|s| s.trim().parse::<u32>().unwrap());

    let mut runs = vec![];
    for run in 0.. {
        let f = create(format!("{run}")).unwrap();
        let lines = &mut lines;
        {
            let mut w = BufWriter::new(&f);
            let mut run = lines.take(RUN_LEN).collect::<Vec<_>>();
            run.sort_unstable();
            for d in run {
                w.write_all(&d.to_ne_bytes()).unwrap();
            }
        }

        runs.push(f);

        if lines.peekable().peek().is_none() {
            eprintln!("{} runs created", runs.len());
            break;
        }
    }

    // ceil(log(K_MERGE, runs))
    for epoch in 0.. {
        let mut new_runs = vec![];
        for (run, files) in runs.chunks(K_MERGE).enumerate() {
            let f = create(format!("merge_{epoch}_{run}")).unwrap();
            {
                let mut w = BufWriter::new(&f);
                let mut heap = MinHeap::with_files(files).unwrap();
                while let Some(d) = heap.pop() {
                    w.write_all(&d.to_ne_bytes()).unwrap();
                }
            }
            new_runs.push(f);
        }

        runs = new_runs;
        if runs.len() <= 1 {
            let out = create("out.txt").unwrap();
            let mut w = BufWriter::new(out);
            let mut r = BufReader::new(&runs[0]);
            r.seek(Start(0))?;
            while let Some(d) = try_read(&mut r).unwrap() {
                writeln!(w, "{d}").unwrap();
            }
            break;
        }
    }

    Ok(())
}

/// A min-heap that keeps track of the source file of the popped element, and inserts a new element
/// from that file if available.
struct MinHeap<'a> {
    heap: BinaryHeap<Reverse<(u32, usize)>>,
    files: Vec<BufReader<&'a File>>,
}

impl<'a> MinHeap<'a> {
    fn with_files(files: &[File]) -> io::Result<MinHeap<'_>> {
        let mut heap = BinaryHeap::default();
        let mut files: Vec<BufReader<&File>> = files.iter().map(BufReader::new).collect();

        // read and insert the first value of each run
        for (i, f) in files.iter_mut().enumerate() {
            f.seek(Start(0))?;
            let d = try_read(f).unwrap().unwrap();
            {
                heap.push(Reverse((d, i)));
            }
        }

        Ok(MinHeap { heap, files })
    }

    fn pop(&mut self) -> Option<u32> {
        if let Some(Reverse((val, i))) = self.heap.pop() {
            // try to refresh
            if let Some(d) = try_read(&mut self.files[i]).unwrap() {
                self.heap.push(Reverse((d, i)));
            }

            Some(val)
        } else {
            None
        }
    }
}

fn create<P: AsRef<Path>>(p: P) -> io::Result<File> {
    File::options()
        .truncate(true)
        .create(true)
        .read(true)
        .write(true)
        .open(p)
}

/// Try to read a u32 value from `f`.
///
/// - Returns `Some(result)` with the result of the IO operation.
/// - Returns `None` if EOF is encountered before the full value is read.
fn try_read(f: &mut BufReader<&File>) -> io::Result<Option<u32>> {
    let d = {
        let mut buf = [0; 4]; // u32 value
        if let Err(e) = f.read_exact(&mut buf) {
            return match e.kind() {
                io::ErrorKind::UnexpectedEof => Ok(None),
                _ => Err(e),
            };
        }
        u32::from_ne_bytes(buf)
    };

    Ok(Some(d))
}
