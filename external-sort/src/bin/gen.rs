use std::{
    env,
    fs::File,
    io::{self, BufWriter, Write},
};

fn main() -> io::Result<()> {
    let args = env::args().collect::<Vec<_>>();
    let fname = &args[1];
    let n = args[2].trim().parse::<u32>().unwrap();

    let f = File::create(fname)?;
    let mut w = BufWriter::new(f);
    for _ in 0..n {
        let d = rand::random::<u32>();
        writeln!(w, "{d}")?;
    }
    Ok(())
}
