use std::{net::SocketAddr, process, str::FromStr};

use clap::Clap;

const KEY_NOT_FOUND: &str = "Key not found";

#[derive(Clap)]
#[clap(name = env!("CARGO_BIN_NAME"),
	   version = env!("CARGO_PKG_VERSION"),
	   author = env!("CARGO_PKG_AUTHORS"),
       about = "Client for a simple key-value store.")]
struct Cli {
    /// IP address of the kvs-server. Accepts an IP address either v4
    /// or v6, and a port number, as `IP:PORT`.
    #[clap(long = "addr", parse(try_from_str), default_value = "127.0.0.1:4000")]
    addr: SocketAddr,
    /// Specify the storage engine to be used.
    #[clap(long = "engine", parse(try_from_str), default_value = "kvs")]
    engine: Engine,
}

enum Engine {
    Kvs,
    Sled,
}

impl FromStr for Engine {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let eng = match s {
            "sled" => Engine::Sled,
            "kvs" => Engine::Kvs,
            _ => return Err(format!("Unknown engine: {}", s)),
        };
        Ok(eng)
    }
}

fn main() -> kvs::Result<()> {
    let cli: Cli = Cli::parse();
    let mut store = kvs::KvStore::open(".")?;

    Ok(())
}
