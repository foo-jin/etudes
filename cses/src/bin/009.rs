//! Your task is to calculate the number of bit strings of length _n_.
//!
//! For example, if n=3, the correct answer is 8, because the possible
//! bit strings are `000`, `001`, `010`, `011`, `100`, `101`, `110`, and `111`.
//!
//! Time limit: 1.00s --  Memory limit: 512 MB
//!
//! # Input
//! The only input line has an integer _n_.
//!
//! # Output
//! Print the result modulo 10⁹+7.
//!
//! # Constraints
//! - 1 ≤ _n_ ≤ 10⁶
//!
//!
//! # Example
//! ```
//! input:  3
//! output: 8
//! ```

use std::io::{BufRead, BufReader};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();

    let mut n: u64 = line.trim().parse().unwrap();
    let mut k = 1;
    const MODULO: u64 = 1_000_000_007;

    while n > 0 {
        let exponent = u64::min(32, n);
        k = (k * 2_u64.pow(exponent as u32)) % MODULO;
        n -= exponent;
    }

    println!("{}", k);
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
