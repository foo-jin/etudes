//! Given an input string of length _n_, print a palindrome of the given string, or print `NO SOLUTION`.
//!
//! Time limit: 1.00s -- Memory limit: 512 MB
//!
//! # Constraints
//! - 1 ≤ _n_ ≤ 10⁶
//!
//! # Example
//! ```
//! Input:  AAAACACBA
//! Output: AAACBCAAA
//! ```

use std::io::{BufRead, BufReader};

/// Let _s_ be the input string. Then, _s_ only has a palindrome if either:
/// - _n_ is even and all character frequencies in _s_ are also even.
/// - _n_ is odd and all character frequencies in _s_ are even, aside from
///   a single character that has an odd frequency.
fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut s = String::new();
    reader.read_line(&mut s).unwrap();

    let mut freq = [0_u32; 26];
    for c in s.trim().chars() {
        let b = (c as u8 - b'A') as usize;
        freq[b] += 1;
    }

    let mut middle = None;
    for (i, f) in freq.iter().enumerate() {
        if f % 2 == 1 {
            if middle.is_none() {
                let c = (i as u8 + b'A') as char;
                middle = Some(c);
            } else {
                println!("NO SOLUTION");
                return;
            }
        }
    }

    let mut front = String::new();
    for (i, f) in freq.iter().enumerate() {
        let c = (i as u8 + b'A') as char;
        for _ in 0..f / 2 {
            front.push(c);
        }
    }

    let back: String = front.chars().rev().collect();
    print!("{}", front);
    if let Some(c) = middle {
        print!("{}", c);
    }
    print!("{}", back);
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
