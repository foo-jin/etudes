//! Given a number _n_, list all 2ⁿ bit strings (a Gray code) of length _n_.
//!
//! Time limit: 1.00s -- Memory limit: 512 MB
//!
//! # Constraints
//! - 1 ≤ _n_ ≤ 16
//!
//! # Example
//! input:
//! ```
//! 2
//! ```
//! output:
//! ```
//! 00
//! 01
//! 11
//! 10
//! ```

use std::io::{BufRead, BufReader};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut s = String::new();
    reader.read_line(&mut s).unwrap();

    let n = s.trim().parse::<u8>().unwrap() as usize;
    for k in 0..(1 << n) {
        // explanation on offbeat.cc
        let gray = k ^ (k >> 1);
        let s = format!("{:016b}", gray);
        let (_, s) = s.split_at(16 - n);
        println!("{}", s)
    }
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!(
        "\ndt:  {}s {}ms {}μs",
        dt.as_secs(),
        dt.as_millis() - (1000 * dt.as_secs() as u128),
        dt.as_micros() - (1000 * dt.as_millis())
    );
    eprintln!("mem: {}", cses::MemoryStats::current())
}
