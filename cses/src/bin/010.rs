//! Your task is to calculate the number of trailing zeros in the factorial _n_!.
//!
//! For example, 20! = 2432902008176640000 and it has 4 trailing zeros.
//!
//! Time limit: 1.00s -- Memory limit: 512 MB
//!
//! # Input
//! The only input line has an integer _n_.
//!
//! # Output
//! Print the number of trailing zeros in _n_!.
//!
//! # Constraints
//! - 1 ≤ _n_ ≤ 10⁹
//!
//! # Example
//! ```
//! input:  20
//! output: 4
//! ```

use std::io::{BufRead, BufReader};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();

    let n: u32 = line.trim().parse().unwrap();
    let mut alpha = 0;

    // we use Legendre's formula
    // so we calculate α = ⌊n/5⌋ + ⌊n/5²⌋ + ⌊n/5³⌋ + ...
    for exp in 1.. {
        let power = 5_u32.pow(exp);
        if power > n {
            break;
        }
        alpha += n / power;
    }

    println!("{}", alpha)
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
