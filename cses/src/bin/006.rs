//! A number spiral is an infinite grid whose upper-left square has
//! number 1. Here are the first five layers of the spiral:
//! ```
//! +--+--+--+--+--+
//! |1 |2 |9 |10|25|
//! +--+--+--+--+--+
//! |4 |3 |8 |11|24|
//! +--+--+--+--+--+
//! |5 |6 |7 |12|23|
//! +--+--+--+--+--+
//! |16|15|14|13|22|
//! +--+--+--+--+--+
//! |17|18|19|20|21|
//! +--+--+--+--+--+
//! ```
//!
//! Your task is to find out the number in row _y_ and column _x_.
//!
//! Time limit: 1.00s -- Memory limit: 512 MB
//!
//! # Input
//! The first input line contains an integer _t_: the number of tests.
//!
//! After this, there are _t_ lines, each containing integers _y_ and _x_.
//!
//! # Output
//! For each test, print the number in row _y_ and column _x_.
//!
//! # Constraints
//! - 1 ≤ _t_ ≤ 10⁵
//! - 1 ≤ _y,x_ ≤ 10⁹
//!
//! # Example
//! input:
//! ```
//! 3
//! 2 3
//! 1 1
//! 4 2
//! ```
//! output:
//! ```
//! 8
//! 1
//! 15
//! ```

use std::{
    io::{BufRead, BufReader},
    num::Wrapping,
};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();

    let n: u64 = line.trim().parse().unwrap();
    for _ in 0..n {
        line.clear();
        reader.read_line(&mut line).unwrap();
        let mut parts = line
            .split_whitespace()
            .map(str::parse::<u64>)
            .filter_map(Result::ok);
        let y: u64 = parts.next().unwrap();
        let x: u64 = parts.next().unwrap();
        println!("{}", unspiral(y, x));
    }
}

fn unspiral(y: u64, x: u64) -> u64 {
    let k = Wrapping(u64::max(y, x));
    let delta = Wrapping(y) - Wrapping(x);
    let one = Wrapping(1);

    // f(k,k) = 1 + 2{1 + 2 + ... + (k-1)}
    // = (arithmetic progression)
    // 1 + 2 * (k-1)k/2 = 1+k(k-1)
    let diag_val = one + k * (k - one);
    let result = if k.0 % 2 == 0 {
        diag_val + delta
    } else {
        diag_val - delta
    };
    result.0
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
