//! A permutation of integers 1,2,…,_n_ is called beautiful if there
//! are no adjacent elements whose difference is 1.
//!
//! Given _n_, construct a beautiful permutation if such a permutation
//! exists.
//!
//! Time limit: 1.00s -- Memory limit: 512 MB
//!
//! # Input
//! The only input line contains an integer n.
//!
//! # Output
//! Print a beautiful permutation of integers 1,2,…,_n_. If there are
//! several solutions, you may print any of them. If there are no
//! solutions, print `NO SOLUTION`.
//!
//! # Constraints
//! - 1 ≤ _n_ ≤ 10⁶
//!
//! # Examples
//! ```
//! input:  5
//! output: 4 2 5 3 1
//! ```
//!
//! ```
//! input:  3
//! output: NO SOLUTION
//! ```

use std::io::{BufRead, BufReader};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();

    let n: u32 = line.trim().parse().unwrap();
    if (2..=3).contains(&n) {
        println!("NO SOLUTION");
        return;
    }

    for i in (2..=n).step_by(2).chain((1..=n).step_by(2)) {
        print!("{} ", i);
    }
    println!()
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
