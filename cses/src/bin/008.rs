//! Your task is to divide the numbers 1,2,…,_n_ into two sets of equal sum.
//!
//! Time limit: 1.00s -- Memory limit: 512 MB
//!
//! # Input
//! The only input line contains an integer _n_.
//!
//! # Output
//! Print `YES`, if the division is possible, and `NO` otherwise.
//!
//! After this, if the division is possible, print an example of how to
//! create the sets. First, print the number of elements in the first set
//! followed by the elements themselves in a separate line, and then,
//! print the second set in a similar way.
//!
//! # Constraints
//! - 1 ≤ _n_ ≤ 10⁶
//!
//!
//! # Examples
//! ```
//! input:  7
//! output:
//! YES
//! 4
//! 1 2 4 7
//! 3
//! 3 5 6
//! ```
//!
//! ```
//! input:  6
//! output: NO
//! ```

use std::{
    io::{BufRead, BufReader},
    mem,
};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();

    let n: u64 = line.trim().parse().unwrap();
    let (mut set, mut set2) = (Vec::new(), Vec::new());
    let (mut sum, mut sum2) = (0, 0);

    for i in (1..=n).rev() {
        if sum > sum2 {
            mem::swap(&mut set, &mut set2);
            mem::swap(&mut sum, &mut sum2)
        }
        set.push(i);
        sum += i;
    }

    if sum != sum2 {
        println!("NO");
        return;
    }

    println!("YES");
    println!("{}", set.len());
    for k in set {
        print!("{} ", k)
    }
    println!("\n{}", set2.len());
    for k in set2 {
        print!("{} ", k)
    }
    println!();
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
