//! Given a string consisting of the characters 'A', 'C', 'G', 'T' (a
//! DNA sequence), find the longest repetition in the sequence.
//!
//! Time limit: 1.00s -- Memory limit: 512MB
//!
//! # Input
//! The only input line contains a string of _n_ characters.
//!
//! # Output
//! Print one integer: the length of the longest repetition.
//!
//! # Constraints
//! - 1 ≤ _n_ ≤ 10⁶
//!
//! # Example
//! ```
//! input   ATTCGGGA
//! output  3
//! ```

use std::io::{BufRead, BufReader};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();

    // X will not occur in the input
    let mut prev = 'X';
    let mut max = 0;
    let mut count = 0;
    for c in line.chars() {
        if c == prev {
            count += 1;
        } else {
            max = u32::max(max, count);
            count = 1;
            prev = c;
        }
    }

    max = u32::max(max, count);
    println!("{}", max)
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
