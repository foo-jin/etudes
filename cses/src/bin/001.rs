//! Consider an algorithm that takes as input a positive integer _n_. If _n_
//! is even, the algorithm divides it by two, and if _n_ is odd, the
//! algorithm multiplies it by three and adds one. The algorithm repeats
//! this, until _n_ is one.
//!
//! The task is to simulate the execution of the algorithm for agiven value of _n_.
//!
//! Time limit: 1.00s -- Memory limit: 512MB
//!
//! # Input
//! The only input line contains an integer _n_.
//!
//! # Output
//! Print a line that contains the sequence of values of _n_ during the execution.
//!
//! # Constraints
//! - 1 ≤ _n_ ≤ 10⁶
//!
//! # Example
//! ```
//! input   3
//! output  3 10 5 16 8 4 2 1
//! ```

use std::{
    io::{BufRead, BufReader},
    time::Instant,
};

fn solve() {
    let mut input = BufReader::new(std::io::stdin());
    let mut line = String::new();
    input.read_line(&mut line).unwrap();

    let mut n = line.trim().parse::<u64>().unwrap();
    print!("{}", n);
    while n != 1 {
        n = if n % 2 == 0 { n / 2 } else { (n * 3) + 1 };
        print!(" {}", n);
    }
    println!();
}

fn main() {
    let start = Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
