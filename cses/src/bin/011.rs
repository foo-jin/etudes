//! You have two coin piles containing _a_ and _b_ coins. On each move, you
//! can either remove one coin from the left pile and two coins from the
//! right pile, or two coins from the left pile and one coin from the
//! right pile.
//!
//! Your task is to efficiently find out if you can empty both the piles.
//!
//! Time limit: 1.00s -- Memory limit: 512 MB
//!
//! # Input
//! The first input line has an integer _t_: the number of tests.
//!
//! After this, there are _t_ lines, each of which has two integers
//! _a_ and _b_: the numbers of coins in the piles.
//!
//! # Output
//! For each test, print `YES` if you can empty the piles and `NO` otherwise.
//!
//! # Constraints
//! - 1 ≤ _t_ ≤ 10⁵
//! - 0 ≤ _a,b_ ≤ 10⁹
//!
//! # Example
//! Input:
//! ```
//! 3
//! 2 1
//! 2 2
//! 3 3
//! ```
//! Output:
//! ```
//! YES
//! NO
//! YES
//! ```

use std::io::{BufRead, BufReader};

// a = 2 * x + y
// b = 2 * y + x
// solve for x, y
//
// a + b = 3(x+y)
// 3(x+y) % 3 = 0
// x = (2b - a) / 3
// y = (2a - b) / 3
//
// necessary conditions:
// a + b mod 3 = 0
// a >= b/2
//
// Assuming the necessary conditions hold, it needs to be shown that the
// values for x and y we get are nonnegative integers.
//
// Details ommitted (see offbeat.cc)
//
// It can be concluded that the necessary conditions are also sufficient.
fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut s = String::new();
    reader.read_line(&mut s).unwrap();

    let _t: u32 = s.trim().parse().unwrap();
    for line in reader.lines().map(Result::unwrap) {
        let parts: Vec<u32> = line
            .split_whitespace()
            .map(str::parse)
            .map(Result::unwrap)
            .collect();
        let a = *parts.iter().min().unwrap();
        let b = *parts.iter().max().unwrap();
        if (a + b) % 3 == 0 && a >= b / 2 {
            println!("YES")
        } else {
            println!("NO");
        }
    }
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
