//! Given a line of _n-1_ numbers in the range _1...n_, find
//! the missing number. All numbers are distinct.
//!
//! Time limit: 1.00s -- Memory limit: 512MB
//!
//! # Input
//! The first input line contains an integer _n_.
//!
//! The second line contains _n-1_ numbers. Each number is distinct and
//! in the range _1...n_ (inclusive).
//!
//! # Output
//! Print the missing number.
//!
//! # Constraints
//! - 2 ≤ _n_ ≤ 2·10⁵
//!
//! # Example
//! input:
//! ```
//! 5
//! 2 3 1 5
//! ```
//! output:
//! ```
//! 4
//! ```

use std::io::{BufReader, Read};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut input = String::new();
    reader.read_to_string(&mut input).unwrap();
    let mut lines = input.lines();
    let n: u32 = lines.next().unwrap().trim().parse().unwrap();
    let mut xs: Vec<u32> = lines
        .next()
        .unwrap()
        .trim()
        .split_whitespace()
        .map(str::parse::<u32>)
        .collect::<Result<_, _>>()
        .unwrap();

    xs.sort_unstable();
    for (i, x) in xs.iter().enumerate() {
        if i as u32 + 1 != *x {
            println!("{}", i + 1);
            return;
        }
    }
    println!("{}", n);
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
