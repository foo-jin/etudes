//! Your task is to count for _k_=1,2,…,_n_ the number of ways two knights
//! can be placed on a _k_ × _k_ chessboard so that they do not attack each
//! other.
//!
//! Time limit: 1.00s -- Memory limit: 512 MB
//!
//! # Input
//! The only input line contains an integer _n_.
//!
//! # Output
//! Print _n_ integers: the results.
//!
//! # Constraints
//! - 1 ≤ _n_ ≤10000
//!
//! # Example
//! input:
//! ```
//! 8
//! ```
//! output:
//! ```
//! 0
//! 6
//! 28
//! 96
//! 252
//! 550
//! 1056
//! 1848
//! ```

use std::io::{BufRead, BufReader};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();

    let n: u64 = line.trim().parse().unwrap();
    for i in 1..=n {
        let isq = i * i;
        let possible = isq * (isq - 1) / 2;
        let peaceful = possible - count_attacking_positions(i);
        println!("{}", peaceful)
    }
}

/**
```txt
/// = 2, ### = 3, \\\ = 4, @ = 6,  = 8
+---+---+---+---+---+---+---+---+---+---+---+
|///|###|\\\|\\\|\\\|\\\|\\\|\\\|\\\|###|///|
+---+---+---+---+---+---+---+---+---+---+---+
|###|\\\| @ | @ | @ | @ | @ | @ | @ |\\\|###|
+---+---+---+---+---+---+---+---+---+---+---+
|\\\| @ |   |   |   |   |   |   |   | @ |\\\|
+---+---+---+---+---+---+---+---+---+---+---+
|\\\| @ |   |   | X |   | X |   |   | @ |\\\|
+---+---+---+---+---+---+---+---+---+---+---+
|\\\| @ |   | X |   |   |   | X |   | @ |\\\|
+---+---+---+---+---+---+---+---+---+---+---+
|\\\| @ |   |   |   | K |   |   |   | @ |\\\|
+---+---+---+---+---+---+---+---+---+---+---+
|\\\| @ |   | X |   |   |   | X |   | @ |\\\|
+---+---+---+---+---+---+---+---+---+---+---+
|\\\| @ |   |   | X |   | X |   |   | @ |\\\|
+---+---+---+---+---+---+---+---+---+---+---+
|\\\| @ |   |   |   |   |   |   |   | @ |\\\|
+---+---+---+---+---+---+---+---+---+---+---+
|###|\\\| @ | @ | @ | @ | @ | @ | @ |\\\|###|
+---+---+---+---+---+---+---+---+---+---+---+
|///|###|\\\|\\\|\\\|\\\|\\\|\\\|\\\|###|///|
+---+---+---+---+---+---+---+---+---+---+---+
n = 11,
4 * 2              if n > 2
+ 8 * 3 / 2          if n > 3
+ 4 * 4 / 2          if n > 3
+ (4 * n-4) * 4 / 2  if n > 3
+ (4 * n-4) * 6 / 2  if n > 3
+ (n-4)^2 * 8 / 2    if n > 3
```
*/
fn count_attacking_positions(n: u64) -> u64 {
    match n {
        0 => unreachable!(),
        1 | 2 => 0,
        3 => 4 * 2,
        4..=u64::MAX => {
            (4 * 2 / 2)
                + (8 * 3 / 2)
                + (4 * 4 / 2)
                + (4 * 4 * (n - 4) / 2)
                + (4 * 6 * (n - 4) / 2)
                + ((n - 4) * (n - 4) * 8 / 2)
        }
    }
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
