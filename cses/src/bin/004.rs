//! Given an array of _n_ integers, modify the array in such a way that
//! the array is increasing, i.e., every element is at least as large as
//! the previous element. Each increment counts as a single move.
//!
//! Find the minimum number of moves required to make the array increasing.
//!
//! Time limit: 1.00s -- Memory limit: 512MB
//!
//! # Input
//! The first input line contains an integer _n_: the size of the array.
//!
//! Then, the second line contains _n_ integers _x₁,...,xₙ_: the contents of the array.
//!
//! # Output
//! Print the minimum number of moves.
//!
//! # Constraints
//! - 1 ≤ _n_ ≤ 2·10⁵
//! - 1 ≤ _xᵢ_ ≤ 10⁹
//!
//! # Example
//! input:
//! ```
//! 5
//! 3 2 5 1 7
//! ```
//! output:
//! ```
//! 5
//! ```

use std::io::{BufRead, BufReader};

fn solve() {
    let mut reader = BufReader::new(std::io::stdin());
    let mut line = String::new();
    reader.read_line(&mut line).unwrap();
    // we disregard the first line
    line.clear();
    reader.read_line(&mut line).unwrap();
    let xs: Vec<u32> = line
        .split_whitespace()
        .map(str::parse)
        .collect::<Result<_, _>>()
        .unwrap();

    let mut moves = 0;
    let mut delta = 0;
    for w in xs.windows(2) {
        let x = w[0] + delta;
        let y = w[1];
        if x > y {
            delta = x - y;
            moves += delta;
        } else {
            delta = 0;
        }
    }

    println!("{}", moves)
}

fn main() {
    let start = std::time::Instant::now();
    solve();
    let dt = start.elapsed();
    eprintln!("dt: {}s {}ms", dt.as_secs(), dt.as_millis());
    eprintln!("mem: {}", cses::MemoryStats::current())
}
